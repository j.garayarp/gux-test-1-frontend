import './App.css';
import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import axios from "axios";
import { format } from "date-fns";

const url = "http://localhost:8000/pacientes/";

class App extends Component{

  state = {
    loading: true,
    error: null,
    data: undefined,
    nroRol: '',
    paciente: '',
    prevision: '',
    estado: '',
    fechaAltaDesde: '',
    fechaAltaHasta: ''
  }

  peticionGet = async () => {
    this.setState({
      loading: true,
      error: null,
    })

    try {
      const params = new URLSearchParams(
        [
          ['nro_rol', this.state.nroRol],
          ['nombre_paciente', this.state.paciente],
          ['prevision', this.state.prevision],
          ['estado', this.state.estado],
          ['fecha_alta_desde', this.state.fechaAltaDesde ? format(this.state.fechaAltaDesde, 'yyyy-MM-dd') : this.state.fechaAltaDesde],
          ['fecha_alta_hasta', this.state.fechaAltaHasta ? format(this.state.fechaAltaHasta, 'yyyy-MM-dd') : this.state.fechaAltaHasta],
        ]
      );
      const res = await axios.get(url, {params});
      this.setState(
        {
          loading: false,
          data: res.data.results
        }
      )
    } catch (error) {
      this.setState(
        {
          loading: false,
          error: error
        }
      )
    }
  }

  componentDidMount() {
    this.peticionGet();
  }

  render() {

    if (this.state.loading) {
      return 'Loading...'
    }

    if (this.state.error) {
      return 'Error 404'
    }

    return (
      <div className="container">
        <div className="row mt-5">
          <div className="col-3 form-group">
            <label>ROL</label>
            <input
              className="form-control"
              onChange={e => this.setState({
                nroRol: e.target.value
              })}
              value={this.state.nroRol}
            ></input>
          </div>
          <div className="col-3 form-group">
            <label>Paciente</label>
            <input 
              className="form-control"
              onChange={e => this.setState({
                paciente: e.target.value
              })}
              value={this.state.paciente}
            ></input>
          </div>
          <div className="col-3 form-group">
            <label>Previsión</label>
            <select 
              className="form-control" 
              onChange={e => this.setState({
                prevision: e.target.value
              })}
              value={this.state.prevision}
            >
              <option value="">Todos</option>
              <option value="cod1">cod1</option>
              <option value="cod2">cod2</option>
            </select>
          </div>
          <div className="col-3 form-group">
            <label>Estado</label>
            <select 
              className="form-control"
              onChange={e => this.setState({
                estado: e.target.value
              })}
              value={this.state.estado}
            >
              <option value="">Todos</option>
              <option value="activo">Activo</option>
              <option value="inactivo">Inactivo</option>
            </select>
          </div>
        </div>
        <div className="row mt-2">
          <div className="col-3 form-group">
            <label>Fecha Alta Desde</label>
            <DatePicker 
              className="form-control" 
              selected={this.state.fechaAltaDesde}
              onChange={(date) => this.setState({
                fechaAltaDesde: date
              })}
            />
          </div>
          <div className="col-3 form-group">
            <label>Fecha Alta Hasta</label>
            <DatePicker 
              className="form-control"
              selected={this.state.fechaAltaHasta}
              onChange={(date) => this.setState({
                fechaAltaHasta: date
              })}
            />
          </div>
        </div>
        <div className="row mt-2">
          <div className="col-12">
            <div className="text-right">
              <button 
                className="btn btn-primary mr-3"
                onClick={() => this.setState({
                  nroRol: '',
                  paciente: '',
                  prevision: '',
                  estado: '',
                  fechaAltaDesde: '',
                  fechaAltaHasta: ''
                })} 
              >
                Limpiar
              </button>
              <button onClick={this.peticionGet} className="btn btn-success">
                Buscar
              </button>
            </div>
          </div>
        </div>
        <div className="row mt-3">
          <table className="table">
            <thead>
              <tr>
                <th>N° Rol</th>
                <th>Nombre Paciente</th>
                <th>Fecha Hosp.</th>
                <th>Fecha Alta</th>
                <th>Código Prest.</th>
                <th>Acción</th>
                <th>Previsión</th>
                <th>Estado</th>
              </tr>
            </thead>
            <tbody>
              {this.state.data.map(paciente => {
                return (
                  <tr key={paciente.id}>
                    <td>{paciente.nro_rol}</td>
                    <td>{paciente.nombre_paciente}</td>
                    <td>{paciente.fecha_hospitalizacion}</td>
                    <td>{paciente.fecha_alta}</td>
                    <td>{paciente.codigo_prestacion}</td>
                    <td>{paciente.accion}</td>
                    <td>{paciente.prevision}</td>
                    <td>{paciente.estado}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default App;
